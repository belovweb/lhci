# prnt-lighthouse-audit
1) склонировать проект
2) npm install
3) npm run multiple-lighthouse-audit

сгенерится папка report, в ней lighthouse в ней jsonы и htmlки и csvшки с отчетами. html можно смотреть для красоты. json можно потом использовать для автоматизации проверки ухудшились/улучшились ли результаты. Также json можно скидывать сюда https://googlechrome.github.io/lighthouse/viewer/ и будет формироваться точно такой же отчет как в html. csv использовать для гугл доков, например

В файле sites.txt указываются сайты, с которых надо снимать метрики

Для прогона по нескольким страницам использую https://github.com/mikestead/lighthouse-batch , были другие варианты (см. полезная инфа), но этот вариант было быстрее и проще сделать.

Убрал из аудита один параметр (charset) из-за этой ошибки - https://github.com/GoogleChrome/lighthouse/issues/10876 . Из-за нее весь показатель best practices становился ?. Если открывать lighthouse в браузере, то этот показатель будет зеленым. Надо мониторить когда решится проблема в issue

Также убрал из аудита показатель PWA (Progressive Web Applications)

Полезная инфа:
1.  https://github.com/GoogleChrome/lighthouse
2.  https://github.com/mikestead/lighthouse-batch
3.  https://blog.toukopeltomaa.com/run-simple-scheduled-lighthouse-tests-on-git-lab-ci
4.  https://medium.com/@iamasamanthaa/the-magic-of-running-multiple-lighthouse-performance-tests-on-macos-4d22ae56621c
5.  https://css-tricks.com/build-a-node-js-tool-to-record-and-compare-google-lighthouse-reports/
6.  https://medium.com/@giezendanenner/running-lighthouse-reports-on-the-command-line-1691a1b06a56