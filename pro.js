const fs = require('fs');
const glob = require('glob');
const path = require('path');

const lighthouse = require('lighthouse');
const chromeLauncher = require('chrome-launcher');
const {
  computeMedianRun,
} = require('lighthouse/lighthouse-core/lib/median-run.js');

const URLS = [
  'https://www.26-2.ru/art/355073-putin-sdelal-vsem-buhgalteram-strany-neojidannyy-podarok',
  'https://www.arbitr-praktika.ru/article/2277-kak-sostavit-zayavlenie-o-privlechenii-k-subsidiarnoy-otvetstvennosti-kontroliruyushchih',
  'https://www.budgetnik.ru/art/102925-qqq-07-10-universalnyy-peredatochnyy-dokument-2019',
  'https://www.budgetnyk.com.ua/article/433-zmna-tsni-za-odinitsyu-tovaru-shcho-vrahuvati-u-dodatkovy-ugod',
  'https://www.buh.mcfr.kz/article/382-elektronnaya-torgovlya',
  'https://www.buhsoft.ru/article/2189-rasschitat-bolnichnyy-list-beremennosti',
  'https://www.business.ru/article/2018-chto-takoe-prodaji-b2c-i-chem-oni-otlichayutsya-ot-b2b',
  'https://www.cultmanager.ru/article/8768-qqv-19-m07-11-realizatsiya-biletov-na-kulturno-dosugovye-meropriyatiya',
  'https://www.cxychet.ru/article/76971-kartochki-snils-otmenili-kak-prinimat-na-rabotu-novenkih-obmenivat-i-vosstanavlivat-dokumenty',
  'https://www.dirklinik.ru/article/479-osnashchenie-priemnogo-otdeleniya-bolnitsy',
  'https://www.dirsalona.ru/article/1312-chto-takoe-sredniy-chek-v-salone-krasoty',
  'https://www.edu.mcfr.kz/article/3650-ou-jylynday-ou-trbie-jmysyn-taldau',
  'https://www.fd.ru/articles/159500-bankovskiy-kredit-printsipy-raznovidnosti-trebovaniya',
  'https://www.gazeta-unp.ru/articles/51769-oplata-envd-za-2-kvartal-2019-srok',
  'https://www.gd.ru/articles/10346-organizatsionno-shtatnaya-struktura',
  'https://www.gkh.ru/article/102698-kakova-rol-uo-tsj-jk-jsk-v-protsesse-vyseleniya-nanimatelya-iz-pomeshcheniya-za-dolgi',
  'https://www.golovbukh.ua/article/7746-kosmetichniy-remont-orendovanih-primshchen-hto-platit-ta-yak-oblkuvati',
  'https://www.gz.mcfr.kz/article/628-rekonstruktsiya-kapitalnyy-tekushchiy-remont',
  'https://www.hr-director.ru/article/67417-distantsionnaya-forma-obucheniya-19-m7',
  'https://www.kadrovik01.com.ua/article/4445-zmni-u-dlovodstv-2019-elektronniy-dokumentoobg',
  'https://www.kadry.mcfr.kz/article/1403-prikaz-po-lichnomu-sostavu-vidy-izdanie-registratsiya',
  'https://www.kdelo.ru/art/385462-prodlenie-polnomochiy-generalnogo-direktora-19-m7',
  'https://www.kom-dir.ru/article/2753-kak-uvelichit-prodaji-v-roznichnom-magazine',
  'https://www.law.ru/article/21552-qqq-17-m5-04-05-2017-uvolnenie-za-nesootvetstvie-zanimaemoy-doljnosti',
  'https://www.med.mcfr.kz/article/836-osb-meditsinaly-bikesn-lauazymdy-nsaulyy',
  'https://www.medsprava.com.ua/article/653-sindrom-profesynogo-vigoryannya-u-medichnih-pratsvnikv',
  'https://www.menobr.ru/article/65635-qqq-m6-19-instruktsiya-po-obhodu-territorii-shkoly',
  'https://www.pedrada.com.ua/article/2667-metodichn-rekomendats-mon-z-vikladannya-predmetv',
  'https://www.pro-goszakaz.ru/article/103405-qqp-19-m7-otklonili-zayavku-na-uchastie-v-zakupke',
  'https://www.pro-personal.ru/article/1097709-17-m11-zayavlenie-na-otpusk-bez-sohraneniya-zarabotnoy-platy-obrazets-2019',
  'https://www.provrach.ru/article/8335-19-m07-11-minimalnyj-assortiment-lekarstvennyh-preparatov-2019-god',
  'https://www.resobr.ru/article/63310-qqq-m6-18-obrazovatelnye-tehnologii-v-dou-po-fgos',
  'https://www.rnk.ru/article/216693-raschet-po-strahovym-vznosam-za-2-kvartal-2019-goda-forma',
  'https://www.sekretariat.ru/article/210238-faksimile-klyuchevye-aspekty-ispolzovaniya-18-m5',
  'https://www.sop.com.ua/article/225-qqq-16-m4-13-04-2016-sanitarnye-normy-mikroklimata-proizvodstvennyh-pomeshcheniy',
  'https://www.stroychet.ru/article/76964-razmeshchat-informatsiyu-v-eisjs-nujno-po-novym-pravilam',
  'https://www.trudohrana.ru/article/103911-19-m7-naryad-dopusk-na-ognevye-raboty',
  'https://www.tspor.ru/article/2452-otvetstvennost-za-narushenie-trebovaniy-ohrany-truda',
  'https://www.ugpr.ru/article/1780-predvaritelnaya-apellyatsionnaya-jaloba',
  'https://www.zarplata-online.ru/art/161562-prikaz-o-zakreplenii-transportnogo-sredstva-za-voditelem---obrazets-2019',
  'https://www.zdrav.ru/articles/4293660830-19-m07-11-medicinskij-registrator',
];
const NUMBER_OF_RUNS = 5;

const calcDiff = (from, to) => {
  return Math.round(((to || 0) * 100) - ((from || 0) * 100));
};

const getContents = (pathStr) => {
  const output = fs.readFileSync(pathStr, 'utf8', (err, results) => {
    return results;
  });
  return JSON.parse(output);
};


const initialHeader = ['', '', '', '']
const initialSubHeader = ['', '', '', '']
const initialFieldName = ['category', 'id', 'title', 'scoreDisplayMode']
const compareReports = async (from, to, url, timestamp, dirName, isInitial, urlName) => {
  // console.log('from', from);
  // console.log('to', to);
  let audits = [];
  if (isInitial) {
    audits.push(
      [...initialHeader, url, '', ''],
      [...initialSubHeader, urlName, '', ''],
      [...initialFieldName, 'Previous release', 'Current release', 'Разница'],
    );
  } else {
    audits.push(
      [url, '', ''],
      [urlName, '', ''],
      ['Previous release', 'Current release', 'Разница'],
    );
  }
  for (let auditObj of Object.keys(to['audits'])) {

    const scoreDiff = calcDiff(
      (from['audits'][auditObj] && from['audits'][auditObj].score || 0),
      (to['audits'][auditObj] && to['audits'][auditObj].score || 0),
    );

    if (isInitial) {
      let auditCategory = '';
      for (let categoryInFromCategory of Object.keys(to['categories'])) {
        const foundRef = to['categories'][categoryInFromCategory].auditRefs.find(item => item.id === to['audits'][auditObj].id);
        if (foundRef) {
          auditCategory = to['categories'][categoryInFromCategory].title;
          break;
        }
      }
      audits.push(
        [
          auditCategory,
          to['audits'][auditObj].id,
          `"${to['audits'][auditObj].title}"`,
          to['audits'][auditObj].scoreDisplayMode,
          Math.round((from['audits'][auditObj] && from['audits'][auditObj].score || 0) * 100),
          Math.round((to['audits'][auditObj] && to['audits'][auditObj].score || 0) * 100),
          scoreDiff,
        ],
      );
    } else {
      audits.push(
        [
          Math.round((from['audits'][auditObj] && from['audits'][auditObj].score || 0) * 100),
          Math.round((to['audits'][auditObj] && to['audits'][auditObj].score || 0) * 100),
          scoreDiff,
        ],
      );
    }
  }

  const businessAuditsFileName = `${dirName}/${timestamp}_business_diffs.csv`;
  const doesBusinessAuditsFileExist = fs.existsSync(businessAuditsFileName);

  let totalFromScore = 0;
  let totalToScore = 0;

  let businessAuditCategories = [url];
  let businessAuditCategoriesTitles = [
    'Сайт',
    'Среднее Score',
    '',
    '',
  ];
  let businessAuditCategoriesSecondaryTitles = [
    '',
    'Previous release',
    'Current release',
    'Разница',
  ];


  for (let businessAuditCategory in to['categories']) {
    const finalScoreDiff = calcDiff(
      (from['categories'][businessAuditCategory] && from['categories'][businessAuditCategory].score || 0),
      (to['categories'][businessAuditCategory] && to['categories'][businessAuditCategory].score || 0),
    );

    totalFromScore += (from['categories'][businessAuditCategory] && from['categories'][businessAuditCategory].score || 0) * 100;
    totalToScore += (to['categories'][businessAuditCategory] && to['categories'][businessAuditCategory].score || 0) * 100;


    if (!doesBusinessAuditsFileExist) {
      businessAuditCategoriesTitles.push(
        `Среднее ${businessAuditCategory}`,
        '',
        '',
      );
      businessAuditCategoriesSecondaryTitles.push(
        'Previous release',
        'Current release',
        'Разница',
      );
    }

    businessAuditCategories.push(
      Math.round((from['categories'][businessAuditCategory] && from['categories'][businessAuditCategory].score || 0) * 100),
      Math.round((to['categories'][businessAuditCategory] && to['categories'][businessAuditCategory].score || 0) * 100),
      finalScoreDiff,
    );
  }

  totalFromScore = Math.round(totalFromScore / 4);
  totalToScore = Math.round(totalToScore / 4);
  const totalScoreDiff = totalToScore - totalFromScore;
  const [businessAuditCategoriesFirst, ...businessAuditCategoriesOthers] = businessAuditCategories;
  businessAuditCategories = [
    businessAuditCategoriesFirst,
    totalFromScore,
    totalToScore,
    totalScoreDiff,
    ...businessAuditCategoriesOthers,
  ];

  if (!doesBusinessAuditsFileExist) {
    const initialData = [
      businessAuditCategoriesTitles.join(','),
      businessAuditCategoriesSecondaryTitles.join(','),
      businessAuditCategories.join(','),
    ]
    // Save to file:
    fs.writeFileSync(businessAuditsFileName, initialData.join('\n'), 'utf-8');
  } else {
    fs.appendFileSync(businessAuditsFileName, '\n' + businessAuditCategories.join(','));
  }

  return audits;
}

const getMedianUrlResult = async ({url, numberOfRuns, timestamp, isInitial}) => {
  const results = [];
  let compareReportsResult;
  for (let i = 1; i <= numberOfRuns; i++) {
    const runnerResult = await getUrlResult({url, iteration: i, timestamp});
    results.push(runnerResult.lhr);
  }
  const median = computeMedianRun(results);
  const dirBaseName = 'report';

  const dirName = `${dirBaseName}`;
  if (!fs.existsSync(dirName)) {
    fs.mkdirSync(dirName);
  }
  const urlObj = new URL(url);
  let urlName = urlObj.host.replace('www.', '');
  if (urlObj.pathname !== '/') {
    urlName = urlName + urlObj.pathname.replace(/\//g, '_');
  }
  const prevReports = glob(`${dirName}/*_${urlName}_median.json`, {
    sync: true
  });
  if (prevReports.length) {
    const dates = [];
    for (const report in prevReports) {
      const fileName = path.parse(prevReports[report]).name;
      // console.log('fileName', fileName);
      const [fileDate, fileUrl] = fileName.split('_');
      // console.log('fileDate', fileDate);
      dates.push({
        date: new Date(fileDate),
        fileName,
        fileUrl,
      });
    }
    const max = dates.reduce(function (a, b) {
      return a.date > b.date ? a : b;
    });
    // console.log('dates', dates);
    // console.log('max', max);
    const recentReportContents = getContents(dirName + '/' + max.fileName + '.json');

    compareReportsResult = await compareReports(recentReportContents, median, max.fileUrl, timestamp, dirName, isInitial, urlName);
  } else {
    console.log('no prevreports length');
  }
  fs.writeFileSync(`${dirName}/${timestamp}_${urlName}_median.json`, JSON.stringify(median, null, ' '));
  return compareReportsResult;
}

const getUrlResult = async ({url, iteration, timestamp}) => {
  let numberOfRetries = 0;
  let runnerResult;
  let chrome;
  do {
    try {
      console.log('start lighthouse url audit - try #' + numberOfRetries);
      console.log('before await chrome');
      chrome = await chromeLauncher.launch({chromeFlags: ['--headless', '--no-sandbox']});
      console.log('after await chrome');
      const options = {logLevel: 'error', output: 'json', port: chrome.port};
      runnerResult = await lighthouse(url, options, {
        extends: 'lighthouse:default',
        settings: {
          onlyCategories: [
            'performance',
            'accessibility',
            'best-practices',
            'seo',
          ],
          skipAudits: [
            'charset',
            'color-contrast',
          ],
        },
      });
      if (
        runnerResult
        && runnerResult.lhr
        && runnerResult.lhr.categories
        && runnerResult.lhr.categories.performance
        && runnerResult.lhr.categories.performance.score
        && (runnerResult.lhr.categories.performance.score * 100) !== 0
      ) {
        numberOfRetries = 100;
      } else {
        console.log('performance was 0');
        // numberOfRetries = numberOfRetries + 1;
        // await chrome.kill();
        // console.log(`numberOfRetries increased to ${numberOfRetries}`);
        throw new Error('performance was 0')
      }
    } catch {
      console.log('failed while auditing url');
      numberOfRetries = numberOfRetries + 1;
      await chrome.kill();
      console.log(`numberOfRetries increased to ${numberOfRetries}`);
    }
  } while (numberOfRetries <= 12)
  if (numberOfRetries === 100) {
    console.log('lighthouse audit was successful');
  } else {
    console.log('lighthouse audit failed for multiple times');
  }
  console.log('after await lighthouse');
  //
  // // `.report` is the HTML report as a string
  const reportJson = runnerResult.report;
  const dirBaseName = 'report';

  const dirName = `${dirBaseName}`;
  if (!fs.existsSync(dirName)) {
    fs.mkdirSync(dirName);
  }
  const urlObj = new URL(url);
  let urlName = urlObj.host.replace('www.', '');
  if (urlObj.pathname !== '/') {
    urlName = urlName + urlObj.pathname.replace(/\//g, '_');
  }
  fs.writeFileSync(`${dirName}/${timestamp}_${urlName}_${iteration}.json`, reportJson);
  console.log('after fs.writeFileSync');
  //
  // // `.lhr` is the Lighthouse Result as a JS object
  console.log('Report is done for', runnerResult.lhr.finalUrl);
  console.log('Performance score was', runnerResult.lhr.categories.performance.score * 100);
  //
  await chrome.kill();
  console.log('after await chrome.kill');

  return runnerResult;
};

(async () => {
  const dirName = 'report';
  const timestamp = new Date().toISOString();
  let audits;
  let isInitial = true;
  for (let url of URLS) {
    const audit = await getMedianUrlResult({url, numberOfRuns: NUMBER_OF_RUNS, timestamp, isInitial});
    if (audit) {
      if (isInitial) {
        isInitial = false;
        audits = audit;
      } else {
        for (let i = 0; i < audits.length; i++) {
          audits[i].push(...audit[i]);
        }
      }
    }
  }
  if (audits && audits.length) {
    const joinedAudits = audits.map(item => item.join(','));
    fs.writeFileSync(`${dirName}/${timestamp}_develop_diffs.csv`, joinedAudits.join('\n'));
  }
})();
