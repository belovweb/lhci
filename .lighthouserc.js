module.exports = {
  ci: {
    collect: {
      url: ['https://www.edu.mcfr.kz/article/3650-ou-jylynday-ou-trbie-jmysyn-taldau'],
      settings: {chromeFlags: '--no-sandbox'},
    },
    upload: {
      target: 'temporary-public-storage',
    },
  },
};
