module.exports = {
  extends: 'lighthouse:default',
  settings: {
    skipAudits: [
      'charset',
    ],
  },
};
